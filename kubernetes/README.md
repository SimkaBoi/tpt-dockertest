# apply config to kubernetes
kubectl apply -f kubernetes/deployment.yml

# get list of deployments
kubectl get deployments

# get list of pods
kubectl get pods

# show pod log
kubectl logs -f <POD_ID>

# show logs, all pods that match selector
kubectl logs -f -l=app=test


# get pods by selector ( app=test )
kubectl get pods -l=app=test

# apply service
kubectl apply -f kubernetes/service.yml
# get services, wait unit EXTERNAL-IP is visible and open that in browser
kubectl get services

